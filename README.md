# Company core
It's my second course work. The conditions and result are below.


В терминах ER-модели описать предметную область системы расчета
заработной платы организации.
Организация платит сотрудникам зарплату в соответствии с штатным
расписанием (должность : сумма). Кроме своих основных обязанностей
(Начальник, Исполнитель, Экспедитор) сотрудник может привлекать
клиентов. Сотруднику платится процент со сделки (5% прибыли например).
Зарплата облагается налогами, число и ставки которых легко меняются со
временем. Система хранит информацию о клиентах фирмы.

##### Система должна эффективно выдавать ответы на следующие запросы:

1. Рассчитать сумму на руки для всех сотрудников за месяц.
2. Рассчитать годовой доход сотрудников.
3. Сумма сделок с определенным клиентом.
4. Сколько прибыли принес сотрудник за год.
