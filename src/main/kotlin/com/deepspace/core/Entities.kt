package com.deepspace.core

import java.time.LocalDate
import java.util.*


abstract class AbstractEntity(
    var id: UUID,
)

class Officer (
    id: UUID,
    var firstName: String,
    var lastName: String,
    var birthDate: LocalDate
) : AbstractEntity(id) {

}